namespace AdventOfCode
{
    public class Day01Part2 : IPuzzle
    {
        private int[] _sonarReport;
        
        public int Run()
        {
            int increments = 0;
            for (int i = 3; i < _sonarReport.Length; i++)
            {
                int previousGroup = _sonarReport[i-1] + _sonarReport[i-2] + _sonarReport[i-3];
                int currentGroup = _sonarReport[i] + _sonarReport[i-1] + _sonarReport[i-2];
                if (previousGroup < currentGroup)
                {
                    increments++;
                }
            }
            return increments;
        }

        public void LoadInput(string[] input)
        {
            _sonarReport = new int[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                _sonarReport[i] = int.Parse(input[i]);
            }
        }
    }
}