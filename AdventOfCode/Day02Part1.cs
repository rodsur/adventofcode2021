namespace AdventOfCode
{
    public class Day02Part1 : IPuzzle
    {
        private string[] _instructions;
        private int _horizontalPos = 0;
        private int _verticalPos = 0;

        public int Run()
        {
            for (int i = 0; i < _instructions.Length; i++)
            {
                string[] instruction = _instructions[i].Split(" ");
                switch (instruction[0])
                {
                    case "forward":
                        _horizontalPos += int.Parse(instruction[1]);
                        break;
                    case "up":
                        _verticalPos -= int.Parse(instruction[1]);
                        break;
                    case "down":
                        _verticalPos += int.Parse(instruction[1]);
                        break;
                }
            }
            return _horizontalPos * _verticalPos;
        }

        public void LoadInput(string[] input)
        {
            _instructions = input;
        }
    }
}