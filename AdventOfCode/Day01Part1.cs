namespace AdventOfCode
{
    public class Day01Part1 : IPuzzle
    {

        private int[] _sonarReport;
        
        public int Run()
        {
            int increments = 0;
            for (int i = 1; i < _sonarReport.Length; i++)
            {
                if (_sonarReport[i - 1] < _sonarReport[i])
                {
                    increments++;
                }
            }
            return increments;
        }

        public void LoadInput(string[] input)
        {
            _sonarReport = new int[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                _sonarReport[i] = int.Parse(input[i]);
            }
        }
    }
}