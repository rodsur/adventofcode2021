using System.IO;
using NUnit.Framework;
using AdventOfCode;

namespace Tests
{
    public class Day01Part1Tests
    {
        private IPuzzle _puzzle;
        private string[] _input;
        [SetUp]
        public void Setup()
        {
            _puzzle = new Day01Part1();
        }

        [Test]
        public void Day01Part1_SampleInput()
        {
            _input = new[] {"199", "200", "208", "210", "200", "207", "240", "269", "260", "263"};
            _puzzle.LoadInput(_input);
            Assert.AreEqual(7,_puzzle.Run());
        }

        [Test]
        public void Day01Part1_ActualInput()
        {
            _input = File.ReadAllLines("inputs/day01p1input");
            _puzzle.LoadInput(_input);
            Assert.AreEqual(1502,_puzzle.Run());
        }
    }
}