using System.IO;
using AdventOfCode;
using NUnit.Framework;

namespace Tests
{
    public class Day01Part2Tests
    {
        private IPuzzle _puzzle;
        private string[] _input;
        [SetUp]
        public void Setup()
        {
            _puzzle = new Day01Part2();
        }

        [Test]
        public void Day01Part2_SampleInput()
        {
            _input = new[] {"199", "200", "208", "210", "200", "207", "240", "269", "260", "263"};
            _puzzle.LoadInput(_input);
            Assert.AreEqual(5,_puzzle.Run());
        }

        [Test]
        public void Day01Part2_ActualInput()
        {
            _input = File.ReadAllLines("inputs/day01p1input");
            _puzzle.LoadInput(_input);
            Assert.AreEqual(1538,_puzzle.Run());
        }
    }
}