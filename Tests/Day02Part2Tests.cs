using System.IO;
using AdventOfCode;
using NUnit.Framework;

namespace Tests
{
    public class Day02Part2Tests
    {
        private IPuzzle _puzzle;
        private string[] _input;
        
        [SetUp]
        public void Setup()
        {
            _puzzle = new Day02Part2();
        }

        [Test]
        public void Day02Part1_SampleInput()
        {
            _input = new[] {"forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"};
            _puzzle.LoadInput(_input);
            Assert.AreEqual(900,_puzzle.Run());
        }
        
        [Test]
        public void Day02Part1_ActualInput()
        {
            _input = File.ReadAllLines("inputs/day02p1input");
            _puzzle.LoadInput(_input);
            Assert.AreEqual(1176514794,_puzzle.Run());
        }
    }
}